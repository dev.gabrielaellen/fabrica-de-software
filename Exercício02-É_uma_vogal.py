vogal = input('Digite uma letra: ')
if vogal in 'a,e,i,o,u,A,E,I,O,U,á,é,í,ó,ú,Á,É,Í,Ó,Ú':
    print(f'A letra {vogal} é uma vogal')
else:
    print(f'A letra {vogal} não é uma vogal')